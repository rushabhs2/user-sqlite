from sqlalchemy import Column, Integer, String, ForeignKey
from database import Base
from sqlalchemy.orm import relationship
from uuid import uuid4


# class Blog(Base):
#     __tablename__ = 'blogs'
#
#     id = Column(Integer, primary_key=True, index=True)
#     title = Column(String)
#     body = Column(String)
#     user_id = Column(Integer, ForeignKey('users.id'))
#
#
# creator = relationship("User", back_populates="blogs")


class UserModel(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True, index=True)
    firstname = Column(String)
    lastname = Column(String)
    username = Column(String)
    password = Column(String)
    email = Column(String)
    mobile = Column(Integer)


otp = relationship("OtpModel", back_populates="creator")


class OtpModel(Base):
    __tablename__ = "otps"

    otp_id = Column(Integer, primary_key=True, index=True)
    otp = Column(String(6))
    user_id = Column(Integer, ForeignKey("users.id"))


creator = relationship("UserModel", back_populates="otp")



# blogs = relationship('Blog', back_populates="creator")
