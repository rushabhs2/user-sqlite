import math

from fastapi import FastAPI, Depends, HTTPException, status

from fastapi.security import OAuth2PasswordRequestForm
from passlib.context import CryptContext
from sqlalchemy.orm import Session
import models
import tokens
import requests

from database import engine, SessionLocal

from models import UserModel, OtpModel
from schemas import UserSchema, getUserSchema, ForgotPasswordSchema, ForgetPasswordVerifyOTPSchema
from random import randint

app = FastAPI()

models.Base.metadata.create_all(engine)


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


@app.post('/login', tags=['Login'])
def login(request: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(get_db)):
    user = db.query(models.UserModel).filter(UserModel.email == request.username).first()

    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"User doesn't exist")

    if not pwd_context.verify(request.password, user.password):
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Invalid password")

    access_token = tokens.create_access_token(data={"sub": user.email})
    return {"access_token": access_token, "token_type": "bearer"}

    # return 'Login successfull'


@app.post('/user', tags=['Users'])
def create_user(request: UserSchema, db: Session = Depends(get_db)):
    existing_useremail = db.query(UserModel).filter(UserModel.email == request.email).first()
    if existing_useremail:
        return "Email already exists"

    existing_username = db.query(UserModel).filter(UserModel.username == request.username).first()
    if existing_username:
        return "Username already exists"

    hashedPassword = pwd_context.hash(request.password)

    new_user = UserModel(firstname=request.firstname, lastname=request.lastname, username=request.username,
                         password=hashedPassword,
                         email=request.email, mobile=request.mobile)

    db.add(new_user)
    db.commit()
    db.refresh(new_user)

    return new_user


@app.get('/users', tags=['Users'])
def get_all(db: Session = Depends(get_db)):
    users = db.query(UserModel).all()
    return users


@app.get('/user/{id}', tags=['Users'], response_model=getUserSchema)
def get_user(id: int, db: Session = Depends(get_db)):
    user = db.query(UserModel).filter(UserModel.id == id).first()

    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"User is not available")
    return user


@app.delete('/user/{id}', tags=['Users'])
def destroy(id: int, db: Session = Depends(get_db)):
    user = db.query(UserModel).filter(UserModel.id == id)
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"User with id {id} not found")
    user.delete(synchronize_session=False)
    db.commit()
    return 'Data deleted successfully'


@app.post('/forgotpassword/{mobile}', tags=['Login'])
def ForgotPassword(request: ForgotPasswordSchema, db: Session = Depends(get_db)):
    user = db.query(UserModel).filter(UserModel.mobile == request.mobile).first()
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"User not found")

    if user:
        otp_model = OtpModel()

        otp = randint(100, 900)
        otp_model.otp = otp
        otp_model.user_id = user.id
        db.add(otp_model)
        db.commit()
        return otp


@app.post('/verifyotp', tags=['Login'])
def verifyotp(request: ForgetPasswordVerifyOTPSchema, db: Session = Depends(get_db)):
    user = db.query(UserModel).filter(UserModel.mobile == request.mobile).first()

    if user:
        user2 = db.query(OtpModel).filter(OtpModel.user_id == user.id).first()

    if not user2.otp == request.otp:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Invalid OTP")
    # else:
    #     return "valid otp"

    if request.new_password != request.confirm_password:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Password is incorrect")
    else:
        user.password = pwd_context.hash(request.new_password)
        # print(request.new_password)
        # print(user.password)

        db.add(user)
        db.commit()
        return f"New password is: {user.password}"
# @app.post('/blog', status_code=status.HTTP_201_CREATED)
# def create(request: schemas.Blog, db: Session = Depends(get_db)):
#     new_blog = models.Blog(title=request.title, body=request.body)
#
#     db.add(new_blog)
#     db.commit()
#     db.refresh(new_blog)
#     return {
#         "title": request.title,
#         "body": request.body
#     }


# @app.get('/blogs', response_model=List[schemas.ShowBlog])
# def get_all(db: Session = Depends(get_db)):
#     blogs = db.query(models.Blog).all()
#     return blogs
#
#
# @app.get('/blog/{id}', status_code=200, response_model=schemas.ShowBlog)
# def show(id, db: Session = Depends(get_db)):
#     blog = db.query(models.Blog).filter(models.Blog.id == id).first()
#     if not blog:
#         raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
#                             detail=f"Blog is not available")
#     return blog
#
#
# @app.delete('/blog/{id}', status_code=status.HTTP_204_NO_CONTENT)
# def destroy(id, db: Session = Depends(get_db)):
#     blog = db.query(models.Blog).filter(models.Blog.id == id)
#     if not blog.first():
#         raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
#                             detail=f"Blog with id {id} not found")
#     blog.delete(synchronize_session=False)
#     db.commit()
#     return 'done'

#
# @app.put('/blog/{id}', status_code=status.HTTP_202_ACCEPTED)
# def update(id, request: schemas.Blog, db: Session = Depends(get_db)):
#     blog = db.query(models.Blog).filter(models.Blog.id == id)
#     if not blog.first():
#         raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
#                             detail=f"Blog with id {id} not found")
#     blog.update(request)
#     db.commit()
#     return 'updated'
