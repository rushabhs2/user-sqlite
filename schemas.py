from pydantic.main import BaseModel

from typing import Optional


# class Blog(BaseModel):
#     title: str
#     body: str


# class ShowUserSchema(BaseModel):
#     id: int
#     firstname: str
#     lastname: str
#     username: str
#     email: str
#     mobile: int


class Config:
    orm_mode = True


#
# class ShowBlog(BaseModel):
#     title: str
#     body: str
#     creator: ShowUser
#
#     class Config:
#         orm_mode = True


class UserSchema(BaseModel):
    firstname: str
    lastname: str
    username: str
    password: str
    email: str
    mobile: str


class LoginSchema(BaseModel):
    username: str
    # mobile: Optional[str] = None
    password: str


class getUserSchema(BaseModel):
    firstname: str
    lastname: str
    username: str
    email: str
    mobile: int

    class Config:
        orm_mode = True


class ForgotPasswordSchema(BaseModel):
    mobile: int

    class Config:
        orm_mode = True


class ForgetPasswordVerifyOTPSchema(BaseModel):
    mobile: int
    otp: str
    new_password: str
    confirm_password: str

    class Config:
        orm_mode = True


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: Optional[str] = None
